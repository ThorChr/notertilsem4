# Machine Learning Explainability

https://www.kaggle.com/learn/machine-learning-explainability

Extract human-understandable insights from any model.


---

- [Use Cases for Model Insights](#use-cases-for-model-insights)
  * [What types of insights are possible](#what-types-of-insights-are-possible)
  * [Why are these insights valuable](#why-are-these-insights-valuable)
    + [Debugging](#debugging)
    + [Informing feature engineering](#informing-feature-engineering)
    + [Directing future data collection](#directing-future-data-collection)
    + [Informing human decision making](#informing-human-decision-making)
    + [Building trust](#building-trust)
- [Permutation Importance](#permutation-importance)
  * [How it works](#how-it-works)
  * [Code example](#code-example)
  * [Interpreting permutation importances](#interpreting-permutation-importances)
- [Partial Plots](#partial-plots)
  * [Partial dependence plots](#partial-dependence-plots)
  * [How it works](#how-it-works-1)
  * [Code example](#code-example-1)
    + [Issues](#issues)
  * [2D partial dependence plots](#2d-partial-dependence-plots)
- [SHAP Values](#shap-values)
  * [How they work](#how-they-work)
  * [Code to calculate SHAP values](#code-to-calculate-shap-values)
- [Advanced Uses of SHAP Values](#advanced-uses-of-shap-values)
  * [Summary plots](#summary-plots)
  * [Summary plots in code](#summary-plots-in-code)
  * [SHAP dependence contrubution plots](#shap-dependence-contrubution-plots)
  * [Dependence contribution plots in code](#dependence-contribution-plots-in-code)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>


---

# Use Cases for Model Insights

https://www.kaggle.com/code/dansbecker/use-cases-for-model-insights

> Why and when do you need insights?

## What types of insights are possible

Noget man ofte hører om Machine Learning er at modellerne er "black boxes", i den forståelse at man kan få gode predictions, men man forstår ikke logikken bag de predictions. Den forståelse er korrekt, i den forstand at mange data scientists ikke har lært hvordan man extracter insights fra modellerne.

Kurset her, og noterne jeg tager der til, vil lave en gennemgang af teknikker der vil give en mulighed for at extracte følgende insights fra sofistikerede machine learning modeller:

* Hvilke features i dataen mente modellen var de vigtigste?
* Hvor enhvert enkelt prediction fra en model, hvordan påvirkede hver eneste feature i dataen den bestemte prediction?
* Hvordan påvirker hver feature modellens predictions i et 'big-picture' sense? Altså, hvad er den typiske effekt af en given feature for den bestemte model?

## Why are these insights valuable

Disse insights har mange use cases, nogle af dem er fx:

* Debugging
* Informing feature engineering
* Directing future data collection
* Informing human decision-making
* Building trust

### Debugging

Verdenen har meget unreliable, disorganiseret og generelt beskidt data. Når man skriver kode for at håndterer preprocessing, så tilføjer man en ekstra kilde for fejl. Tilføj potentiale for target leakage, og så er det normen fremfor undtagelsen, at have en hvis mængde fejl i ens project.

Given frekvensen og de potentielle slemme konsekvenser af bugs, er debugging en af de mest værdifulde skills man kan have inden for data science. Forståelse af mønstre en model finder, vil hjælpe en med at identificere når disse går imod ens personlige kendskab til verdenen - hvilket ofte er det første skridt i tracking down bugs.

### Informing feature engineering

Feature engineering er normalt den mest effektive måde for at forbedre ens models præcision. Feature engineering involvere konstant opbyggelse af nye features ved at gøre brug af transformations på ens data eller features man tidligere har lavet.

Nogle gange kan man gøre dette, ved kun at gøre brug af ens forståelse af den virkelige verden, men hvis man arbejder med et data set der har flere hundrede features (eller arbejder med et domæne man ikke selv har professionel forståelse inden for), så bliver dette meget hurtigt besværligt - hvis ikke umuligt.

Et eksempel er en Kaggle competition der havde mange hundrede features, der hed ```f1```, ```f2```, ```f3```, ```f4``` osv... Der var en competitor der fandt ud af at forskellen mellem to features ```f527``` og ```f528``` var med til at danne en ny og meget powerful feature. Modeller der gjorde brug af denne nye feature, klarede sig meget bedre end dem der ikke gjorde.

Forståelse af ens model, og extracting human understandable forklaringer, er med til at hjælpe en til at lave sådan nogle nye features.

### Directing future data collection

Man har ingen kontrol over de datasets man downloader online, men mange firmaer og organisationer der gør brug ad data science har mulighed for udvide hvilke typer af data de samler ind. Indsamling af ny data kan være en dyr eller inconvenient handling, så det skal kun gøres når man ved det er det værd. Hvilket er noget som model-based insights kan hjælpe en med.

### Informing human decision making

Nogle beslutninger er automatisk foretaget af modeller. Amazon gør fx ikke brug af mennesker for at foretage beslutninger om hvilke anbefalinger af produkter der skal vises til kunder. Dog er der stadigt mange andre vigtige beslutninger der er foretaget af mennesker. Og for disse beslutninger, kan insights være meget mere værdifulde end bare rene predictions.

### Building trust

Mange mennesker vil ikke helt stole på ens model for vigtige beslutninger, uden at foretage verificering af nogle basale fakta. Hvilket er en god precautiong at have, når man tænker på frekvensen af data errors. I praksis, så vil fremvisning af insights der passer til deres generelle forståelse af problemet, hjælpe med at bygge tillid til modellen, selv blandt folk der ikke har stor knowledge af data science.

# Permutation Importance

Et af de mest basale spørgsmål vi kan stille til vores model er: Hvilke features har den største impakt på vores predictions?

Dette koncept hedder **feature importance.** 

Der er mange forskellige måder at måle feature importance. Nogle tilgange besvarer subtilt forskellige versioner af spørgsmå¨let ovenfor. Andre tilgange har dokumenteret mangler.

I denne omgang vil der blive fokuseret på **permutation importance**. Sammenligning med de fleste andre tilgange, så er permutation importance:

* Hurtig af udregne.
* Alment benyttet og forstået.
* Konsistent med properties vi vil have en feature importance måling at have (??? Sounds weird, men det var hvad der stod.).

## How it works

Permutation importance gør brug af modeller på en helt anden måde ens hvad der ellers er blevet gennemgået så vidt. Dette betyder også at mange mennesker finder det forvirrende til at starte med. 

Tænk på data med den følgende format:

| Height at age 20 (cm) | Height at age 10 (cm) | ... | Socks owned at age 10 |
|-----------------------|-----------------------|-----|-----------------------|
| 182                   | 155                   | ... | 20                    |
| 175                   | 147                   | ... | 10                    |
| ...                   | ...                   | ... | ...                   |
| 156                   | 142                   | ... | 8                     |
| 153                   | 130                   | ... | 24                    |

*<sub>Tabel genereret af https://www.tablesgenerator.com/markdown_tables </sub>*

Vi vil gerne predict en persons højde når de bliver 20 år gammel, ved at gøre brug af data der er tilfængelig ved 10 års alderen.

Vores data inkludere brugbare features (height at age 10), features med lille predictive power (socks owned), og nogle andre features der ikke vil blive fokuseret på.

**Permutation importance er udregnet efter en model er blevet fitted.** <br />
> So we won't change the model or change what predictions we'd get for a given value of height, sock-count, etc.

Istedet vil vi stille det følgende spørgsmål: *Hvis jeg shuffler en tilfældig kolonne af validerings dataen, og laver target og alle andre kolonner være, hvordan vil det påvirke præcisionen af mine predictions i den nu shuffled-data?*

![Image showing the shuffeling of the height at age 10 colomn](https://i.imgur.com/vFICMgf.png)

Denne shuffeling af en enkel kolonne burde sørge for at vores model er mindre præcis, siden at den resulterende data ikke er tilsvarende noget der er observeret i den virkelige verden. Præcisionen falder i sær af hvis vi shuffler en kolonne vores model har meget stærkt afhængig af. Hvis vi istedet havde shufflet ```socks owned``` så havde det ikke påvirket predictions nær så meget.

Med den insight, så er processen således:

1. Skaf en trænet model.
2. Shuffle værdierne i en enkel kolonne, lav predictions med det resulterende dataset. Brug disse predictions og de sande target værdier, for at udregne hvor stort et tab vores model led grundet denne shuffeling. Performance tabet indikerer vigtigheden af den variable vi lige har shuffled.
3. Returner dataen til den originale rækkefølge (undoing step 2). Gentag nu step 2 med den næste kolonne i datasettet, indtil man har udregnet vigtigheden af hver eneste kolonne.

## Code example

Vores eksempel vil gøre brug af en model der forudsiger hvorvidt om et fodbold hold vil have the "Man of the Game" winner, baseret på holdets statistikker. Man of the game er givet til den bedste spiller i spillet.  Model-building er ikke fokuspunktet her, så nedenstående kode loader data og bygger en basal model.

```python
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier

data = pd.read_csv('../input/fifa-2018-match-statistics/FIFA 2018 Statistics.csv')
y = (data['Man of the Match'] == "Yes")  # Convert from string "Yes"/"No" to binary
feature_names = [i for i in data.columns if data[i].dtype in [np.int64]]
X = data[feature_names]
train_X, val_X, train_y, val_y = train_test_split(X, y, random_state=1)
my_model = RandomForestClassifier(n_estimators=100,
                                  random_state=0).fit(train_X, train_y)
```

Her er hvordan man udregner og viser vigtigheden, med bibloteket [eli5](https://eli5.readthedocs.io/en/latest/)

```python
import eli5
from eli5.sklearn import PermutationImportance

perm = PermutationImportance(my_model, random_state=1).fit(val_X, val_y)
eli5.show_weights(perm, feature_names = val_X.columns.tolist())
```

| Weight 		   | Feature 					|
|------------------|----------------------------|
| 0.1750 ± 0.0848  |	Goal Scored				|
| 0.0500 ± 0.0637  |	Distance Covered (Kms)	|
| 0.0437 ± 0.0637  |	Yellow Card 			|
| 0.0187 ± 0.0500  |	Off-Target				|
| 0.0187 ± 0.0637  |	Free Kicks				|
| 0.0187 ± 0.0637  |	Fouls Committed			|
| 0.0125 ± 0.0637  |	Pass Accuracy %			|
| 0.0125 ± 0.0306  |	Blocked					|
| 0.0063 ± 0.0612  |	Saves					|
| 0.0063 ± 0.0250  |	Ball Possession %		|
| 0 ± 0.0000 	   |		Red 				|
| 0 ± 0.0000 	   |		Yellow & Red 		|
| 0.0000 ± 0.0559  |	On-Target				|
| -0.0063 ± 0.0729 |	Offsides				|
| -0.0063 ± 0.0919 |	Corners					|
| -0.0063 ± 0.0250 |	Goals in PSO 			|
| -0.0187 ± 0.0306 |	Attempts 				|
| -0.0500 ± 0.0637 |	Passes 					|

## Interpreting permutation importances

Værdierne mod toppen er de mest vigtige features, og dem mod bunden var mindst betydning.

Det første tal i hver række viser hvor meget model performance decreasede når der blev gjort brug af en random shuffling.

Som de fleste ting i data science, så er der noget randomness til den præcise performance change ved at shuffle en kolonne. Hvis man vil have et mere præcist tal, så skal man gentage processen med mange forskellige shuffles. Tallet efter ± indikerer hvormeget performance varierede med fra et resuffle til et andet.

Nogle gange vil man se negative værdier for permutation importances. I de tilfælde, så er ens shuffles data mere præcis end den sande data. Det sker når den bestemte feature ikke nogen betydning for modellen har, men med ren tilfældighed så blev den omgang bare mere præcis. Det er common i mindre datasets, fordi der er mere plads til luck/chance.

I vores tilfælde kan man se at den vigtigste feature var **Goals scored.** Det virker meget realistisk. Fodbolds fans kan måske have lidt mere intuition om de andre features er korrekt, eller om der er noget overraskelse deriblandt.

# Partial Plots

https://www.kaggle.com/code/dansbecker/partial-plots

> How does each feature affect your predictions? 

## Partial dependence plots

Feature importance viser hvilke features der mest påvirker vores predictions. Partial dependence plots viser *hvordan* en feature påvirker vores predictions.

Dette er brugbart til at besvare spørgsmål som:

* Controlling for all other house features, what impact do longitude and latitude have on home prices? To restate this, how would similarly sized houses be priced in different areas?
* Are predicted health differences between two groups due to differences in their diets, or due to some other factor?

## How it works

Lige som permutation importance, så er peatial dependence plots udregner efter at en model er blevet fit.

I vores fodbod eksempel, så er teamsne forskellige i mange måder: Hvor mange passes de har lavet, skud de har taget, mål de scorede osv. Ved første blik, kan det være svært at afkoble effekten af de features.

For at se hvordan partial plots seperere effekten af hver feature, så starter vi med at tænke på en enkel række af data. Fx den række kan repræsentere et team der har haft bolden 50% af tiden, lavede 100 passes, tog 10 skud og scorede 1 mål.

Vi vil gøre brug af vores model til at predicte udfaldet. Men vi vil **gentagene gange ændre værdien for en variable** for at lave en serie af predictions. Vi kunne predict udfaldet hvis teamet havde bolden kun 40% af gangene. Bagefter 50%, så 60% osv. Vi tracer vores udfald på den vertikale akse, og værdien af vores feature langs den horisontale akse.

Det kan så blive gentaget for alle rows, og så tager man average af ens resultat.

## Code example

Nedenstående kode laver en simpel model.

```python
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier

data = pd.read_csv('../input/fifa-2018-match-statistics/FIFA 2018 Statistics.csv')
y = (data['Man of the Match'] == "Yes")  # Convert from string "Yes"/"No" to binary
feature_names = [i for i in data.columns if data[i].dtype in [np.int64]]
X = data[feature_names]
train_X, val_X, train_y, val_y = train_test_split(X, y, random_state=1)
tree_model = DecisionTreeClassifier(random_state=0, max_depth=5, min_samples_split=5).fit(train_X, train_y)
```

Visual representation af vores model.

```python
from sklearn import tree
import graphviz

tree_graph = tree.export_graphviz(tree_model, out_file=None, feature_names=feature_names)
graphviz.Source(tree_graph)
```

![Visuel representation af modellen vi lavede ovenover.](https://i.imgur.com/WAgauFj.png)

Og nu kommer vi til kode delen for oprettelsen af vores Partial Dependence Plot. Denne laver vi ved at gøre brug af bibloteket [PDPBox](https://pdpbox.readthedocs.io/en/latest/)

```python
from matplotlib import pyplot as plt
from pdpbox import pdp, get_dataset, info_plots

# Create the data that we will plot
pdp_goals = pdp.pdp_isolate(model=tree_model, dataset=val_X, model_features=feature_names, feature='Goal Scored')

# plot it
pdp.pdp_plot(pdp_goals, 'Goal Scored')
plt.show()
```

![Resultat af ovenstående kode.](https://i.imgur.com/ylkFZi1.png)

Der er nogle ting der er værd at pointere ved denne graf.

* y-aksen er fortolket som **ændring i ens prediction** fra hvad det ville have predicted ved baseline (eller den mest venstre) værdien.
* Det blå område indikere vores level of condidence.

Fra denne graf, kan vi se at score et mål markant øger ens sandsynlighed for at vinde "Man of The Match" premien, men ekstra mål efter det, ser ikke ud til at have stor effekt på ens predictions.

### Issues

Nogle modeller er meget simple, hvilket betyder der kommer problemer når man forsøger af lave Partial Dependence Plots. Et eksempel er når vi forsøger at lave et plot over "Distance Convered (Kms)".

![Distance Convered (Kms)](https://i.imgur.com/O3cEHpA.png)

Den her graf er alt for simpel til at representere virkeligheden. Men det er som sagt fordi vores model er simpel. 

Nedenstående er grafen for den samme feature, men lavet med en Random Forest model.

![Distance Convered (Kms) med random forest model](https://i.imgur.com/0X88I45.png)

Det ser noget bedre ud end vores tidligere resultat. Dog skal vi være obs på at vores dataset er ret lille, så vi skal være forsigtig i hvordan vi fortolker nogen som helst model.

## 2D partial dependence plots

Hvis man er nysgerrig vedrørende interaktionernene mellem features, så er 2D partial dependence plots meget brugbare.

Vi vil igen gøre brug af vores Decision Tree model.

```python
# Similar to previous PDP plot except we use pdp_interact instead of pdp_isolate and pdp_interact_plot instead of pdp_isolate_plot
features_to_plot = ['Goal Scored', 'Distance Covered (Kms)']
inter1  =  pdp.pdp_interact(model=tree_model, dataset=val_X, model_features=feature_names, features=features_to_plot)

pdp.pdp_interact_plot(pdp_interact_out=inter1, feature_names=features_to_plot, plot_type='contour')
plt.show()
```

![PDP interact for "Goal Scored" and "Distance Covered (Kms)"](https://i.imgur.com/syjuewK.png)

Grafen viser predictions for any kombination af mål scoret og distance covered.

Fx kan vi se at de højeste predictions når et team scorer mindst 1 mål, og løber en total distance tæt på 100km. Hvis de scorer 0 mål, så har distance ingen betydning.

# SHAP Values

https://www.kaggle.com/code/dansbecker/shap-values

> Understand individual predictions

Du har set (og brugt) teknikker til at extract generel information fra en machine learning model. Men hvad hvis man ønskede at bryde det ned hvordan modellen fungerer for en individuel prediction?

SHAP values (Shapley Additive Explanations) bryder en prediction ned, for at vise impakten af hver feature. Hvor kunne man gøre brug af det?

* En model siger at en bank ikke skal låne en person penge, og banken er legally required til at forklare på hvilket basis at denne rejection sker. Så de skal sige "grundet X, Y og Z siger vi nej".
* En healthcare provider ønsker at identificere hvilke faktorer der øger en patients risici for nogle diseases, så de kan tilgå disse risici faktorer direkte, men targeted health interventions.

## How they work

SHAP values fortolker impakten af at have en bestemt værdi for en given feature, i sammenligning med den prediction vi ville have lavet, hvis vores feature have en bestemt baseline værdi.

I de her kurser er der blevet arbejdet med et fodbold dataset, for at finde hvem der ville være Man of the Match. Det vil vi fortsætte med.

Vi kunne spørge:
* Hvor meget var en prediction driven af det faktum at et team scorede 3 mål?

Men det er lettere at give konkrete numeriske svar hvis vi omformulere vores spørgsmål:
* Hvor meget var en prediction driven af det faktum at et team scorede 3 mål, fremfor et bestemt baseline antal af mål?

Dette er self noget der kan blive gentaget for alle andre features as well.

SHAP values gør det i en måde der garantere en nice property. Specifically you decompose a prediction with the following equation:

```sum(SHAP values for all features) = pred_for_team - pred_for_baseline_values```

SHAP values af alle features bliver summet op, for at forklare hvorfor ens prediction var anderledes end ens baseline. Det giver os mulighed for at decompose en prediction i en graf som denne:

![SHAP value decompose of prediction](https://i.imgur.com/Pw0sHiM.png)

Hvordan fortolker man så den graf?

Vores prediction var 0.7, hvorimod Base_value var 0.4979. Feature værdier der er skyld i den øgede prediction er markeret i pink, og deres visuelle størrelse viser magnituden af den features effekt. Features der formindsker vores prediction er markeret i blå. Den største impakt kommer fra at ```Goal Scored``` har en værdi af 2. Ball posession har også en meningfuldeffekt, bare i modsatte retning.

Hvis man minusser længen af de blå bars fra længen af de pinke bars, så bliver det ligmed distancen fra base_value til ens output.

## Code to calculate SHAP values

For at udregne SHAP values bliver der gjort brug af bibloteket [Shap](https://github.com/slundberg/shap)

Nedenstående kode initialisere en ny model baseret på Random Forest.

```python
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier

data = pd.read_csv('../input/fifa-2018-match-statistics/FIFA 2018 Statistics.csv')
y = (data['Man of the Match'] == "Yes")  # Convert from string "Yes"/"No" to binary
feature_names = [i for i in data.columns if data[i].dtype in [np.int64, np.int64]]
X = data[feature_names]
train_X, val_X, train_y, val_y = train_test_split(X, y, random_state=1)
my_model = RandomForestClassifier(random_state=0).fit(train_X, train_y)
```

Der vil blive kigget på SHAP værdier for en enkel række i vores dataset (vi valgte række 5). For kontekst, lad os lige kigge på de rå predictions før vi kigger på SHAP values.

```python
row_to_show = 5
data_for_prediction = val_X.iloc[row_to_show]  # use 1 row of data here. Could use multiple rows if desired
data_for_prediction_array = data_for_prediction.values.reshape(1, -1)


my_model.predict_proba(data_for_prediction_array)
```

```array([[0.29, 0.71]])```

Teamet har altså 70% sandsynlighed for at have en spiller der vinder awaden.

Nedenstående kode er hvordan man får SHAP values for den prediction.

```python
import shap  # package used to calculate Shap values

# Create object that can calculate shap values
explainer = shap.TreeExplainer(my_model)

# Calculate Shap values
shap_values = explainer.shap_values(data_for_prediction)
```

```shap_values``` objektet er en liste med to arrays. Det første array er SHAP values for et negativt outcome (vinder ikke award), pg det andet array er listen af SHAP values for det positive outcome (vinder award). Vi vil gerne vide prediction for det positive outcome, så vi tager fat i det andet array.

```python
shap.initjs()
shap.force_plot(explainer.expected_value[1], shap_values[1], data_for_prediction)
```

![SHAP representation of prediction row 5.](https://i.imgur.com/E0n3d6M.png)

Hvis vi tager et kig i koden, kan vi se vi referere Trees i ```shap.TreeExplainer(my_model)```. Dog er det ikke den eneste model SHAP bibloteket har explainers for.

* ```shap.DeepExplainer``` virker med Deep Learning modeller.
* ```shap.KernelExplainer``` virker med alle modeller, dog så er det langsommere end andre explainers, og den tilbyder kun en approximation, fremfor de præcise værdier.

Nedenstående kode viser hvordan man ville få fat i den tidligere viste graf, ved at gøre brug af KernelExplainer.

```python
# use Kernel SHAP to explain test set predictions
k_explainer = shap.KernelExplainer(my_model.predict_proba, train_X)
k_shap_values = k_explainer.shap_values(data_for_prediction)
shap.force_plot(k_explainer.expected_value[1], k_shap_values[1], data_for_prediction)
```

# Advanced Uses of SHAP Values

https://www.kaggle.com/code/dansbecker/advanced-uses-of-shap-values

> Aggregate SHAP values for even more detailed model insights

SHAP values viser hvor meget en given feature ændrede vores prediction (sammenlignet med hvis vi lavede den prediction ved en baseline værdi af den feature).

## Summary plots

Permutation importance er god fordi den giver en simpel numerisk måling for at se hvilke features der har betydning til en model.

Men den fortælle ikke hvordan hver feature betyder noget. Hvis en feature har medium importance, så kunne det betyde at den har: 

* en stor effekt for et par få predictions, men udover det basically ingen effekt, eller
* en medium effekt for alle predictions.

SHAP summary plots giver os et birds-eye perspektiv over feature importance, og hvad der står bag det.

![SHAP summary plot over fodbold dataset.](https://i.imgur.com/TKciah0.png)

Denne graf er lavet af mange dots. hvert dot har 3 karakterisika:

* Vertical lokalitet viser hvilken feature den visuallisere.
* Farve viser omend den feature var høj eller lav, for den række af datasettet.
* Horizontal lokalitet viser om effekten af den værdi, er skyd i en højere eller lavere prediction.

Eksempel, dottet i top venstre side, var for et team der scorede få mål, hvilket reducerede prediction med 0.25.

## Summary plots in code

Nedenstående er 2 kode blokke, den ene er for at loade dataset og model, den anden er for at lave summary plot baseret på SHAP values.

```python
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier

data = pd.read_csv('../input/fifa-2018-match-statistics/FIFA 2018 Statistics.csv')
y = (data['Man of the Match'] == "Yes")  # Convert from string "Yes"/"No" to binary
feature_names = [i for i in data.columns if data[i].dtype in [np.int64, np.int64]]
X = data[feature_names]
train_X, val_X, train_y, val_y = train_test_split(X, y, random_state=1)
my_model = RandomForestClassifier(random_state=0).fit(train_X, train_y)
```

```python
import shap  # package used to calculate Shap values

# Create object that can calculate shap values
explainer = shap.TreeExplainer(my_model)

# calculate shap values. This is what we will plot.
# Calculate shap_values for all of val_X rather than a single row, to have more data for plot.
shap_values = explainer.shap_values(val_X)

# Make plot. Index of [1] is explained in text below.
shap.summary_plot(shap_values[1], val_X)
```

Nedenstående graf er resultat af den sidste kode blok.

![SHAP Summary plt](https://i.imgur.com/m9LoeoW.png)

Obs. Det kan være meget langsomt at udregne SHAP values. Det er ikke et problem her, fordi det er et lille dataset, men med større sets, skal man være mere opmærksom. En undtagelse er hvis man bruger en ```xgboost``` model, fordi SHAP har nogle optimeringer for det, og er derfor meget hurtigere.

## SHAP dependence contrubution plots

Vi har før brug Partial Dependence Plots for at vise hvordan en enkel feature impakter predictions. Disse er gode og giver god relevant information for mange real-world use cases. Plus med lille ekstra arbejde, kan de blive forklaret til et ikke-teknisk publikum.

Men der er meget de ikke viser. Fx hvad er distributionen af effekter? Er effekten af at have en bestemt værdi rimelig konstant, eller vareiere det meget, afhængig af værdierne af andre features? SHAP dependence contribution plots giver en insight der minder om PDP, men de tilføjer meget ekstra detalje.

![SHAP dependence construbition plot](https://i.imgur.com/JYS4OT4.png)

Start med at fokusere på formen, og så tænker vi på farve lidt senere. Hvert eneste dot repræsentere en række af dataen. Den horizontale lokalitet er den reale værdi fra datasettet, og den vertikale lokalitet viser effekten af at have denne værdi, på ens prediction. (y-akse = Effect on prediction, by having a specific value. x-akse = The specific value from the dataset.)

The spread suggest at andre faktorer må interagere med Ball Posession %. Fx her er der blevet highlighted to dots der har en rimelig ens Ball Posession % værdi, men med en meget drastisk forskel i deres SHAP value. 

![SHAP dots highlighted](https://i.imgur.com/7cGsUfB.png)

## Dependence contribution plots in code

Koden for at lave et dependence contribution plot minder meget om grafer vi tidligere har lavet. Forskellen er den sidste linje kode:

```python
import shap  # package used to calculate Shap values

# Create object that can calculate shap values
explainer = shap.TreeExplainer(my_model)

# calculate shap values. This is what we will plot.
shap_values = explainer.shap_values(X)

# make plot.
shap.dependence_plot('Ball Possession %', shap_values[1], X, interaction_index="Goal Scored")
```

Hvis man ikke giver et argument for ```interaction_index``` så vil Shapley gøre brug af noget logik, for at udvælge en der måske er interessent.

Der går ikke meget kode til at lave de ting her, men tricket er at tænke kritisk over ens resultater, og ikke så meget at skrive koden.

## Exercise stuff

![SHAP Summary from exercise](https://i.imgur.com/p3Yoh8Y.png)

Exercise starter ud med et summaryplot, der bruges til netop at give et summary. Den giver mulighed for nemt at se hvilke features der har høj betydning og sammenligne forskellige features overfladisk med hinanden.

Hvis man finder en feature som ```num_lab_procedures``` der er meget jumbled sammen, så kan det være fordi denne feature hænger sammen med en anden feature. For at finde ud af hvilken feature den hænger sammen med, kan man gøre brug af et SHAP contribution dependence plot.

![SHAP contribution dependence plot](https://i.imgur.com/QwtSZqc.png)

Her kan man se at når ```feature_of_interest``` har en værdi [1.0 : 1.4] så kan man sige at en højere værdi fra ```other_feature``` vil betyde en mere positiv impakt på prediction. Når man krydser 1.5 for feature of interest, så bliver dette vendt på hovedet og er nu omvendt.

 