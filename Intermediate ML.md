# Intermediate Machine Learning 

- [Intermediate Machine Learning](#intermediate-machine-learning)
- [Missing Values](#missing-values)
  * [Scoring of the different methods](#scoring-of-the-different-methods)
    + [Drop Columns with Missing Values](#drop-columns-with-missing-values)
    + [Imputation](#imputation)
    + [An Extension to Imputation](#an-extension-to-imputation)
    + [Why is imputation better](#why-is-imputation-better)
  * [Exercise](#exercise)
- [Categorical Variables](#categorical-variables)
  * [How to handle categorical variables](#how-to-handle-categorical-variables)
  * [Recognize categorical variables in your dataset](#recognize-categorical-variables-in-your-dataset)
  * [Execution of the different approaches](#execution-of-the-different-approaches)
    + [Drop categorical variables](#drop-categorical-variables)
    + [Ordinal Encoding](#ordinal-encoding)
    + [One-Hot Encoding](#one-hot-encoding)
  * [Which approach is the best](#which-approach-is-the-best)
  * [Exercise](#exercise-1)
- [Pipelines](#pipelines)
  * [Construction of a pipeline](#construction-of-a-pipeline)
    + [Define Preprocessing Steps](#define-preprocessing-steps)
    + [Define the Model](#define-the-model)
    + [Create and Evaluate the Pipeline](#create-and-evaluate-the-pipeline)
  * [Conclusion af pipeline](#conclusion-af-pipeline)
  * [Use of a pipeline](#use-of-a-pipeline)
- [Cross-Validation](#cross-validation)
  * [Why use cross-validation](#why-use-cross-validation)
  * [Code example of cross-validation](#code-example-of-cross-validation)
- [XGBoost](#xgboost)
  * [Gradient Boosting](#gradient-boosting)
  * [Parameter Tuning](#parameter-tuning)
    + [n_estimators](#n-estimators)
    + [early_stopping_rounds](#early-stopping-rounds)
    + [learning_rate](#learning-rate)
    + [n_jobs](#n-jobs)
  * [Conclusion](#conclusion)
  * [Exercise: XGBoost](#exercise--xgboost)
- [Data Leakage](#data-leakage)
  * [Target Leakage](#target-leakage)
    + [Prevent target leakage](#prevent-target-leakage)
  * [Train Test Contamination](#train-test-contamination)
  * [Conclusion](#conclusion-1)
- [Done.](#done)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>


# Missing Values

https://www.kaggle.com/alexisbcook/missing-values

Der er nogle forskellige måder man kan håndtere manglende værdier i ens datasæt.

* A Simple Option: Drop Columns with Missing Values <br/>
![Visual representation of cropping columns with missing values.](https://i.imgur.com/iVCyroq.png) <br />
> Unless most values in the dropped columns are missing, the model loses access to a lot of (potentially useful!) information with this approach. As an extreme example, consider a dataset with 10,000 rows, where one important column is missing a single entry. This approach would drop the column entirely!

* A Better Option: Imputation <br />
![Imputation](https://i.imgur.com/SSevvVo.png) <br />
> **Imputation** fills in the missing values with some number. For instance, we can fill in the mean value along each column. 

* An Extension To Imputation <br />
![Visual representation of Extension To Imputation](https://i.imgur.com/X3x0wGs.png) <br />
> In this approach, we impute the missing values, as before. And, additionally, for each column with missing entries in the original dataset, we add a new column that shows the location of the imputed entries.

## Scoring of the different methods

For at kunne sige hvilken en der er bedst, har man brug for at teste det, og se hvilken betydning de forskellige tilgange rent faktisk har.

### Drop Columns with Missing Values
![Score for drop columns](https://i.imgur.com/5ZiN2Mx.png)<br/>
Her bliver der vist kodemæssigt hvordan man ville oprette en liste der indeholder navnene på alle kolonner der har entries der mangler noget data. <br/>
Her er man så også opmærksom på at droppe de samme kolonner i både trænings og validerings setsne.

### Imputation

![Score from Approach 2 (Imputation)](https://i.imgur.com/VvT4ZhG.png)

Her kan man se at vi fik en MAE på 178166. <br/>
Vores MAE fra approach 1, lå på 183550, hvilket er en del højere (~5%). Dette betyder at **approach 2, altså imputation, klarede sig bedre**.


### An Extension to Imputation

![Score from Approach 3 (An Extension to Imputation)
](https://i.imgur.com/j35xycr.png)

For det her dataset / model, så resulterede denne approach i en værre MAE end approach 2. Ikke med meget, men stadigt en anelse værre. og considering det ekstra arbejde for at lave det, så kan man erklærere det som et tab.

### Why is imputation better

Træningsdaten har ~11.000 rows, og 12 colomns, hvor 3 kolonner indeholder manglende data. For hver af de 3 kolonner, så er det under 50% af entries der mangler.<br/>
Det betyder at man mister rigtigt, rigtigt mange informationer om ens data, når man bare dropper det. Og ML handler om at recognize mønstre ud fra data, så hvis der er mindre data at arbejde med, så påvirker det ens models mulighed for at gøre sit job.


## Exercise

**MAE (Drop columns with missing values):** *17837.82570776256*

**MAE (Imputation):** *18062.894611872147*

> Given that thre are so few missing values in the dataset, we'd expect imputation to perform better than dropping columns entirely. However, we see that dropping columns performs slightly better! While this can probably partially be attributed to noise in the dataset, another potential explanation is that the imputation method is not a great match to this dataset. That is, maybe instead of filling in the mean value, it makes more sense to set every missing value to a value of 0, to fill in the most frequently encountered value, or to use some other method.

Så ye, i det her dataset fik vi altså en dårligere MAE ved at gøre brug af imputation. Det kan være fordi der er nogle finurligheder ved det her datasæt, der simpelthen bare gør at det er et dårligt match for imputation. <br/>
For det meste er imputation bedst, men det er ikke altid sagen. Hvilket er hvorfor man netop skal køre sådan en slags test, for at tjekke hvad man burde gøre.


# Categorical Variables

https://www.kaggle.com/alexisbcook/categorical-variables

Categorical Variables er entries / værdier / svar, der falder ind under kategorier. Her er to eksempler:

* En forespørgel er sendt rundt der spørger hvor ofte man spiser morgenmad, man har 4 svarmuligheder: "Aldrig", "Sjældent", "De fleste dage" eller "Hver eneste dag".<br>
De svar man får her, vil være categorical, fordi de svar vil falde ind under et *"fixed"* sæt af kategorier.

* Hvis folk svarede i en survey om hvilket bilmærke de ejede, så ville deres svar falde ind i kategorier som fx "Honda", "Toyota", "Nissan" osv. Hvert bilmærke er altså en kategori for sig selv.

## How to handle categorical variables

Ligesom ved "Missing Values", så er der også nogle forskellige muligheder her. 

* Drop Categorical Variables<br />
Den helt klart letteste tilgang. Alle kolonner der indeholder categorical variables (*kategoriske variabler?*) bliver simpelthen helt fjernet fra ens dataset.

* Ordinal Encoding <br />
**Ordinal encoding** tildeler hver unik værdi et forskelligt heltal.<br />
![Ordinal Encoding visual](https://i.imgur.com/Uushat7.png) <br />
> This assumption makes sense in this example, because there is an indisputable ranking to the categories. Not all categorical variables have a clear ordering in the values, but we refer to those that do as ordinal variables. For tree-based models (like decision trees and random forests), you can expect ordinal encoding to work well with ordinal variables.<br/><br/>
Noget man skal være opmærksom på, er at en ordinal encoder vil generere et unikt heltal for hver unik værdi der er i **trænings dataen**. Hvis at validerings dataen indeholder værdier der ikke også er i træningsdataen, så vil ens encoder give os en fejl, for der er værdier der ikke har et heltal tildelt til dem.

* One-Hot Encoding <br />
**One-hot encoding** laver nye kolonner, der indikere tilstedeværelse (eller fravær) for hver af de mulige værdier i den originale data. <br />
![One-hot encoding visual](https://i.imgur.com/jXl3IvQ.png) <br />
Forskellen mellem One-Hot og Ordinal, handler om at Ordinal antager at der er en ranking blandt ens categorical variables. Hvis man tænker på morgenmads eksemplet, så er der en klar ranking fra aldrig at spise morgenmad, til altid at spise det. Modsat, så har vores bilmærke eksempel ingen ranking (hvilket betyder, at der skulle man gøre brug af One-Hot Encoding).<br />
> In the original dataset, "Color" is a categorical variable with three categories: "Red", "Yellow", and "Green". The corresponding one-hot encoding contains one column for each possible value, and one row for each row in the original dataset. Wherever the original value was "Red", we put a 1 in the "Red" column; if the original value was "Yellow", we put a 1 in the "Yellow" column, and so on. <br/>
> One-hot encoding generally does not perform well if the categorical variable takes on a large number of values (i.e., you generally won't use it for variables taking more than 15 different values).

## Recognize categorical variables in your dataset
En måde man kan skaffe en liste over alle ens categorical variables i ens trænings data er ved at tjekke data typen for hver kolonne.<br/>
Hvis en kolonne har datatypen *object* så indikerer det at den kolonne inkluderer tekst.<br />
**Please note:** Bare fordi en kolonne er *object* betyder det ikke at den er categorical. Det er den ofte, men ikke altid. 

Nedenstående kodeudsnit viser hvordan man finder categorical variables, baseret på ovenstående beskrivelse. 

```python
# Get list of categorical variables
s = (X_train.dtypes == 'object')
object_cols = list(s[s].index)

print("Categorical variables:")
print(object_cols)
```
```
Categorical variables:
['Type', 'Method', 'Regionname']
```

## Execution of the different approaches

Denne del er til for at inkludere kode snippits for at vise hvordan hver af de 3 approaches kan blive udført.

### Drop categorical variables

Her gør man brug af en indbygget funktion (fra pandas / DataFrames) der gør det muligt at vælge kolonner ud fra deres datatype. Vi inkluderer her en parameter, der siger den skal ekskludere en kolonne hvis det er af dtype = object.

```python
drop_X_train = X_train.select_dtypes(exclude=['object'])
drop_X_valid = X_valid.select_dtypes(exclude=['object'])

print("MAE from Approach 1 (Drop categorical variables):")
print(score_dataset(drop_X_train, drop_X_valid, y_train, y_valid))
```

MAE from Approach 1 (Drop categorical variables): *175703.48185157913*

### Ordinal Encoding

Python bibloteket *SciKit-Learn* har en OrdinalEncoder klasse der kan blive brugt til at skaffe ordinal encodings. Vi lopper over vores categorical variables og gør brug af vores ordinal encoder seperat til hver kolonne.

> Scikit-learn has a OrdinalEncoder class that can be used to get ordinal encodings. We loop over the categorical variables and apply the ordinal encoder separately to each column.

```python
from sklearn.preprocessing import OrdinalEncoder

# Make copy to avoid changing original data 
label_X_train = X_train.copy()
label_X_valid = X_valid.copy()

# Apply ordinal encoder to each column with categorical data
ordinal_encoder = OrdinalEncoder()
label_X_train[object_cols] = ordinal_encoder.fit_transform(X_train[object_cols])
label_X_valid[object_cols] = ordinal_encoder.transform(X_valid[object_cols])

print("MAE from Approach 2 (Ordinal Encoding):") 
print(score_dataset(label_X_train, label_X_valid, y_train, y_valid))
```
MAE from Approach 2 (Ordinal Encoding): *165936.40548390493*

> In the code cell above, for each column, we randomly assign each unique value to a different integer. This is a common approach that is simpler than providing custom labels; however, we can expect an additional boost in performance if we provide better-informed labels for all ordinal variables.

Man kan komme ud i problemer hvor ens træningsdata og ens valideringsdata ikke har de samme unikke entries. Dette kan give problemer med encoding. Det mest simple fiks, er simpelthen at droppe de problematiske kolonner.

```python
# Categorical columns in the training data
object_cols = [col for col in X_train.columns if X_train[col].dtype == "object"]

# Columns that can be safely ordinal encoded
good_label_cols = [col for col in object_cols if 
                   set(X_valid[col]).issubset(set(X_train[col]))]
        
# Problematic columns that will be dropped from the dataset
bad_label_cols = list(set(object_cols)-set(good_label_cols))
        
print('Categorical columns that will be ordinal encoded:', good_label_cols)
print('\nCategorical columns that will be dropped from the dataset:', bad_label_cols)
```

### One-Hot Encoding
> We use the OneHotEncoder class from scikit-learn to get one-hot encodings. There are a number of parameters that can be used to customize its behavior.

> To use the encoder, we supply only the categorical columns that we want to be one-hot encoded. For instance, to encode the training data, we supply X_train[object_cols].

```python
from sklearn.preprocessing import OneHotEncoder

# Apply one-hot encoder to each column with categorical data
OH_encoder = OneHotEncoder(handle_unknown='ignore', sparse=False)
OH_cols_train = pd.DataFrame(OH_encoder.fit_transform(X_train[object_cols]))
OH_cols_valid = pd.DataFrame(OH_encoder.transform(X_valid[object_cols]))

# One-hot encoding removed index; put it back
OH_cols_train.index = X_train.index
OH_cols_valid.index = X_valid.index

# Remove categorical columns (will replace with one-hot encoding)
num_X_train = X_train.drop(object_cols, axis=1)
num_X_valid = X_valid.drop(object_cols, axis=1)

# Add one-hot encoded columns to numerical features
OH_X_train = pd.concat([num_X_train, OH_cols_train], axis=1)
OH_X_valid = pd.concat([num_X_valid, OH_cols_valid], axis=1)

print("MAE from Approach 3 (One-Hot Encoding):") 
print(score_dataset(OH_X_train, OH_X_valid, y_train, y_valid))
```

MAE from Approach 3 (One-Hot Encoding): *166089.4893009678*

Noget man skal tænke på med den her tilgang, er *cardinality*. Dette er simpelthen hvor mange entries der er i et sæt. I denne kontekst handler cardinality om hvor mange unikke entries der er i en categorical kolonne.

Årsagen til at man skal tænke på det, er at hvis en kolonne har en cardinality på 25, altså den kolonne har 25 unikke entries, så vil man være nødt til at generere 25 ekstra kolonner, hvis man ønsker at gøre brug af One-Hot encoding her.

Af denne årsag, gør man typisk kun brug af one-hot encode mod kolonner der har en relativt lav cardinality. Hvilket betyder at de andre kolonner (dem med høj cardinality) enten kan blive droppet fra ens dataset, eller man kan gøre brug af ordinal encoding.

Nedenstående er noget Python der demonstrere hvordan man kan holde styr på dette.

```python
# Columns that will be one-hot encoded
low_cardinality_cols = [col for col in object_cols if X_train[col].nunique() < 10]

# Columns that will be dropped from the dataset
high_cardinality_cols = list(set(object_cols)-set(low_cardinality_cols))

print('Categorical columns that will be one-hot encoded:', low_cardinality_cols)
print('\nCategorical columns that will be dropped from the dataset:', high_cardinality_cols)
```

## Which approach is the best
I dette tilfælde så er approach 1 den værste. De to andre havde en meget ens MAE, så der er ingen *meaningful benefit* for at bruge den ene over den anden.

Typisk så vil one-hot encoding (approach 3) være den bedste, og at droppe categorical columns (approach 1) vil være den værste. Det varierer dog fra dataset til dataset.

## Exercise

**MAE from Approach 1 (Drop categorical variables):** *17837.82570776256*

**MAE from Approach 2 (Ordinal Encoding):** *17098.01649543379*

**MAE from Approach 3 (One-Hot Encoding):** *17525.345719178084*

# Pipelines

https://www.kaggle.com/alexisbcook/pipelines

Well, hvad er en pipeline? Well, ifølge det kursus vi følger, så er det:

> Pipelines are a simple way to keep your data preprocessing and modeling code organized. Specifically, a pipeline bundles preprocessing and modeling steps so you can use the whole bundle as if it were a single step.

Så kort fortalt, så er det altså en "måde" (mere en arbejdesfremgang) hvor man kan kombinere preprocessing (altså, håndtering af data fx manglende data osv) og modeling, så man kan bruge hele kombinationen som om det var ét enkelt skridt.

Man kan arbejde med Machine Learning uden at gøre brug af pipelines, dog så er der visse fordele ved at gøre brug af pipelines:

1. **Clearner Code:** Ens kode kan blive meget beskidt, hvis man skal holde styr på ens data ved hvert eneste skridt i løbet af ens preprocessing. Ved at gøre brug af pipelines, behøver man ikke manuelt at holde styr på ens trænings og validerings data for hvert skridt.
2. **Fewer Bugs:** Der er færre muligheder hvor man kan komme til at skrive noget forkert ind. Hvilket betyder, færre bugs.
3. **Easier to Productionize:** Det kan være meget svært at gå fra en prototype model, til at kunne gå til noget der er deployable ved en stor skala. Pipelines kan hjælpe her.
4. **More Options for Model Validation:** *ikke rigtigt noget tekst her, udover at dette er noget man vil arbejde med i næste tutorial*

## Construction of a pipeline

Når man ønsker at konstruere en fuld pipeline, så gøres det i 3 skridt.

### Define Preprocessing Steps
>Similar to how a pipeline bundles together preprocessing and modeling steps, we use the ColumnTransformer class to bundle together different preprocessing steps. 

Nedenstående kode:
  * imputes manglende værdier i **numerisk** data, og
  * imputes manglende værdier og anvender en *one-hot encoding* til **categorical** data.

```python
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import OneHotEncoder

# Preprocessing for numerical data
numerical_transformer = SimpleImputer(strategy='constant')

# Preprocessing for categorical data
categorical_transformer = Pipeline(steps=[
    ('imputer', SimpleImputer(strategy='most_frequent')),
    ('onehot', OneHotEncoder(handle_unknown='ignore'))
])

# Bundle preprocessing for numerical and categorical data
preprocessor = ColumnTransformer(
    transformers=[
        ('num', numerical_transformer, numerical_cols),
        ('cat', categorical_transformer, categorical_cols)
    ])
```

### Define the Model
Det næste skridt, er at definere en *random forest model*.

```python
from sklearn.ensemble import RandomForestRegressor

model = RandomForestRegressor(n_estimators=100, random_state=0)
```

### Create and Evaluate the Pipeline
Til sidst gør vi brug af vores Pipeline klasse så vi kan definere en pipeline der kombinere preprocessing og modelling steps sammen. Der er et par vigtige ting at være opmærksom på:

* Med vores pipeline, så håndtere vi preprocess af trænings data, og vi 'fit' vores model i en enkelt linje kode (uden vores pipeline, så skulle man manuelt håndtere imputation, one-hot encoding og model traning i seperate steps. Dette bliver meget hurtigt beskidt kode, i sær hvis man skal håndtere både numeriske og categorical variables).

* Med en pipeline, så kan man give den "unprocessed features". Dette sker ved at give vores ```predict()``` funktion en parameter der indeholder vores ```X_valid```, hvor sidtnævnte ikke er blevet preprocessed endnu (hvis man ikke gjorde brug af en pipeline, så skulle man selv huske at lave preprocessing på ens validerings data).

```python
from sklearn.metrics import mean_absolute_error

# Bundle preprocessing and modeling code in a pipeline
my_pipeline = Pipeline(steps=[('preprocessor', preprocessor),
                              ('model', model)
                             ])

# Preprocessing of training data, fit model 
my_pipeline.fit(X_train, y_train)

# Preprocessing of validation data, get predictions
preds = my_pipeline.predict(X_valid)

# Evaluate the model
score = mean_absolute_error(y_valid, preds)
print('MAE:', score)
```
```MAE: 160679.18917034855```

## Conclusion af pipeline
Basically, pipelines gør livet meget lettere. Man slipper for selv at holde styr på alle de forskellige skridt, hvilket betyder der er langt mindre mulighed for at begå en dum fejl.

> Pipelines are valuable for cleaning up machine learning code and avoiding errors, and are especially useful for workflows with sophisticated data preprocessing.

## Use of a pipeline

Fra interactive-tutorial, så skal man nu selv definere valid preprocessing steps og en random forest model.

Her opsætter vi nogle skridt som vores pipeline skal udføre. Der er noget bestemt der skal ske for alle numeriske værdier, og noget andet der skal ske for categorical variables.<br/>
Dog for at håndtere vores kategoriske værdier, så skal denne også igennem en pipeline (pipeline er bare en måde at kombinere flere skridt sammen).

```python
# Preprocessing for numerical data
numerical_transformer = SimpleImputer(strategy='median') 

# Preprocessing for categorical data
categorical_transformer = Pipeline(steps=[
    ('imputer', SimpleImputer(strategy='most_frequent')), 
    ('onehot', OneHotEncoder(handle_unknown='ignore'))
])

# Bundle preprocessing for numerical and categorical data
preprocessor = ColumnTransformer(
    transformers=[
        ('num', numerical_transformer, numerical_cols),
        ('cat', categorical_transformer, categorical_cols)
    ])

# Define model
model = RandomForestRegressor(n_estimators=150, random_state=1)
```

Ovenstående endte med at give en MAE på:

```MAE: 17383.64189497717```

Da dette blev submitted til den tilhørende competition, gav det en public score på: *16190.36721* <br/>
Dette er en forbedring fra den tidligere.


# Cross-Validation
https://www.kaggle.com/alexisbcook/cross-validation

Inden for Machine Leaning, handler det jo om at få computeren til at lære nye ting baseret på et mønster, og så evaluere dette. Jo større evaluerings dataset man har, jo mindre noise er der når man laver vurderingen, men for at få et støtte evaluerings set, kræver det at man fjerner data fra selve trænings datasettet, hvilket betyder en værre model.
<br/>Dette er et problem der bliver taklet med cross validation.

I cross validering kører man modeling processen på forskellige subsets af vores data, for at få flere målinger af modellens kvalitet. Nedenstående er et billede der illustrere konceptet.

![cross validation](https://i.imgur.com/12koKnC.png)

## Why use cross-validation

Cross-validation giver en mere præcis vurdering af modellens kvalitet. Der er dog en pris at betale for denne forbedring, nemlig pris. Fremfor kun at gøre en model, hvis man gør brug af ovenstående eksempel, så kører man og træner 5 modeller. 

Så hvornår skal man bruge hvad?

* For små datasets, hvor noget ekstra computational burden, ikke er en stor ting, så bør man bruge cross-validation.
* For store datasets, så er et enkelt validerings set tilstrækkeligt. Koden vil også køre hurtigere.

Der er ingen nem klassificering af hvad der er "store" og "små" datasets. Men generelt, hvis ens model tager et par minutter eller mindre at køre, så er det nok værd at gøre brug af cross-validation.

## Code example of cross-validation

Using a function, we can change a parameter of the model in a loop, and then get the best value.
```python
def get_score(n_estimators):
    
    myy_pipeline = Pipeline(steps=[
        ('preprocessor', SimpleImputer()),
        ('model', RandomForestRegressor(n_estimators = n_estimators, random_state=0))
    ])
    
    scooores = -1 * cross_val_score(myy_pipeline, X, y, cv=3, scoring='neg_mean_absolute_error')
    return scooores.mean();

# Use the funtion and store the results.
results = {}

for i in [50, 100, 150, 200, 250, 300, 350, 400]:
    results[i] = get_score(i)
```

After this, we can visualize the result like this:

```python
import matplotlib.pyplot as plt
%matplotlib inline

plt.plot(list(results.keys()), list(results.values()))
plt.show()
```

![Image showing the result of the previous code block.](https://i.imgur.com/okarpI0.png)


# XGBoost

https://www.kaggle.com/code/alexisbcook/xgboost

*"The most accurate modelling technique for structured data.*

XGBoost står for Extreme Gradient Boosting, hvilket er en implementering af Gradient Boosting, med adskillige ekstra features tilføjet, der fokusere på performance og hastighed.

## Gradient Boosting
Dette er en metode der går igennem cyklusser for iterativt at tilføje modeller ind i en *"ensemble"*.

En ensemble metode er en metode der kombinere predictions af adskillige modeller (fx adskillige træer, for en random forest).

Gradient Boosting starter ud med at initialisere the ensemble med en enkel mode. Denne models predictions kan være meget naïve (selv hvis de er forkerte, vil dette blive fikset senere).

Derefter starter vi cyklusen:

* Først, vi gør brug af den nuværende ensemble til at genere predictions for hver observation i datasettet. 
* Disse predictions bliver brugt til at udregne en *loss function* (ligesom mean squared error).
* Så gør vi brug af den loss function, til at "fit" en ny model, der vil blive tilføjet til vores ensemble. Helt præcist, vi bestemmer model parametre så vi ikke forøger vores loss, ved at tilføje den her model.
* Til sidst, så tilføjer vi den nye model til vores ensemble, og...
* ...Repeat!

![Visuel oversigt over gradient boosting](https://i.imgur.com/C0ZID6V.png)

## Parameter Tuning
XGBoost har en del parameter man kan lege rundt med. Nedenstående er en liste af parameter der er meget vigtigt at kunne.

### n_estimators
```n_estimators``` specificere hvor mange gange at man skal gå igennem model cyklussen beskrevet ovenstående. Det svarer basically til, hvor mange modeller man vil have i ens ensemble.

* For lav en værdi forudsager *underfitting*, hvilket fører til upræcise predictions på både trænings- og testdata.
* For høj en værdi forudsager *overfitting*, hvilket fører til præcise predictions på træningsdaten, men upræcise på test dataen (hvilket er hvad der faktisk betyder noget).

Typiske værdier, ligger mellem 100 - 1000. Dog så varierer dette meget baseret på ```learning_rate``` parametren, som bliver snakket om lidt længere nede.

Her er koden for at angive antallet af modeller i en ensemble:

```python
my_model = XGBRegressor(n_estimators=500)
my_model.fit(X_train, y_train)
```

### early_stopping_rounds
```early_stopping_rounds``` giver en mulighed for automatisk at finde den idealle værdi for ```n_estimators```. Early stopping gør at modellen stopper med at iterere når at validerings scoren stopper med at blive forbedret, selv hvis vi ikke er ved det "hard stop" for ```n_estimators```. Det er smart at sætte en høj værdi for ```n_estimators``` og så gøre brug af ```early_stopping_rounds``` for at finde den optimale tid til at stoppe med at iterere.

Eftersom at by random chance, der nogle gange kommer en runde hvor at scoren ikke er forbedre, så er vi nødt til at angive hvor mange runder der skal gå uden en forbedring, før at vi stopper vores process. 5 er et godt bud: ```early_stopping_rounds=5```

Når man gør brug af ```early_stopping_rounds``` sp er man også nødt til at sætte noget data til side, for at kalkulere validerings scoren - hvilket er gjort ved at sætte ```eval_set``` parametren.

Følgende kode er en modificering til ovenstående, hvor vi inkludere ```early_stopping_rounds```

```python
my_model = XGBRegressor(n_estimators=500)
my_model.fit(X_train, y_train, 
             early_stopping_rounds=5, 
             eval_set=[(X_valid, y_valid)],
             verbose=False)
```

### learning_rate

Fremfor at få predictions ved blot at tilføje predictions sammen fra hver component model, så kan man gange predictions fra hver model, med et lille tal (hvilket er vores learning rate), før vi summer dem sammen.

Det betyder at hvert ekstra tre hjælper os mindre. Hvilket betyder vi kan sætte et højere tal for vores ```n_estimators``` uden at frygte overfitting. Ved så yderligere at gøre brug af ```early_stopping``` finder vi automatisk det optimale antal af estimators.

Generelt, så vil en lille learning rate og et stort antal estimators give de bedste resultater, dog vil det også tage længere tid at træne modellen, eftersom der er mange flere interationer den skal igennem.

Nedenstående kode viser hvordan learning rate kan blive koblet sammen med de tidligere eksempler:

```python
my_model = XGBRegressor(n_estimators=1000, learning_rate=0.05)
my_model.fit(X_train, y_train, 
             early_stopping_rounds=5, 
             eval_set=[(X_valid, y_valid)], 
             verbose=False)
```

### n_jobs

Ved store datasets hvor kørselstiden har betydning, så kan man gøre brug af ¨paralellisme for at bygge ens modeller hurtigere. Det er normalt at sætte parameteren ```n_jobs``` til antallet af cores på ens computer. Dog vil dette ikke hjælpe på mindre datasets.

Den resulterende model vil ikke være bedre, men det hjælper med hastigheden når man arbejder med store dataset.

Nedenstående er et eksempel:

```python
my_model = XGBRegressor(n_estimators=1000, learning_rate=0.05, n_jobs=4)
my_model.fit(X_train, y_train, 
             early_stopping_rounds=5, 
             eval_set=[(X_valid, y_valid)], 
             verbose=False)
```

## Conclusion

XGBoost er basically *the* library man bruger, for at arbejde med "standard tabular data", hvilket er den slags data man gemmer i Pandas DataFrames (modsat en mere eksotisk type data, som billeder og videoer).

## Exercise: XGBoost

Ved at gøre brug af en XGBRegressor med random_state=0, fik jeg en MAE på: ```17662.736729452055```<br/>
Dette var med default parametre.

Step 2, handler nu om at forbedre den model ved at ændre på parametrene.

Ved at gøre brug af den ovenstående kode, fra sektionen om ```n_jobs``` fik jeg forbedret MAE'en ned til: ```16802.965325342466```

Step 3, handler om at ødelægge vores model. Så med vilje lave en model der er værre. Jeg endte med at gøre brug af følgende model

```python
my_model_3 = XGBRegressor(random_state=0, n_estimators=20, learning_rate=5)
```

Hvilket gav mig en MAE på ```1.5474090036664445e+17``` såååe, meget dårligt, men jeg gjorde hvad opgaven ville have. <br/>
Den model var dårlig fårdi der var så få modeller at arbejde med, og de første par modeller er meget naïve i den her algoritme.

# Data Leakage

https://www.kaggle.com/code/alexisbcook/data-leakage

Data Leakage (også bare kaldet Leakage) sker når ens trænings data indeholder informationer om the target, men similar data vil ikke være tilgængelig når modellen senere bliver brugt til at lave predictions. Dette fører til høj performance på trænings dataen (og måske også validerings dataen), men betyder at modellen vil perform poorly i production.

I andre ord: Leakage får en model til at se accurate ud, indtil at man begynder at lave beslutninger med den model, og så bliver den pludslig meget upræcis.

Der er to typer af leakage:

- Target Leakage.
- Train-test contamination.

## Target Leakage
Dette sker når ens predictors inkludere data som ikke er tilgængelig når man laver predictions. 

> It is important to think about target leakage in terms of the timing or chronological order that data becomes available, not merely whether a feature helps make good predictions.

Kaggle har følgende eksempel. <br/>
Forestil dig, du vil gerne kunne predict hvilke personer der ender med at blive syg med pneunomia. De øverste par rækker i ens data, ser således ud:

| got_pneunomia | age | weight | male | took_antibiotic_medicine |
|---------------|-----|--------|------|--------------------------|
| False         | 65  | 100    | False| False                    |
| False         | 72  | 130    | True | False                    |
| True          | 58  | 100    | False| True                     |

Her er problemet, at ens model ville kigge på den sidste kolonne, og tro at den kolonne har stor betydning for om folk ender med at få pneunomia. Problemer er dog bare at, mennesker tager antibiotic medicin EFTER at man har fået pneunomia. <br/>
Dette betyder altså at værdien for den kolonne, ofte bliver ændret efter man har været syg. Dette er target leakage.

> People take antibiotic medicines after getting pneumonia in order to recover. The raw data shows a strong relationship between those columns, but took_antibiotic_medicine is frequently changed after the value for got_pneumonia is determined. This is target leakage.

Modellen ville altså se at alle der har en værdi af ```False``` for ```took_antibiotic_medicine``` ikke havde pneunomia. Så modellen vil som sagt, tro at der er en kobling mellem de to ting.

### Prevent target leakage

To prevent this type of data leakage, any variable updated (or created) after the target value is realized should be excluded.

![Picture showing target leakage](https://i.imgur.com/FrvDzhp.png)

## Train Test Contamination

Dette er en anden form for leakage der sker, når man ikke er forsigtig nok med at adskille trænings data fra validerings data.

Validering er ment som en beregning for at sige hvor god modellen er på data den ikke har set før. Man kan corrupt den proces, hvis at validerings dataen har betydning for preprocessing opførslen.  

> You can corrupt this process in subtle ways if the validation data affects the preprocessing behavior.

> For example, imagine you run preprocessing (like fitting an imputer for missing values) before calling train_test_split(). The end result? Your model may get good validation scores, giving you great confidence in it, but perform poorly when you deploy it to make decisions.

> If your validation is based on a simple train-test split, exclude the validation data from any type of fitting, including the fitting of preprocessing steps. This is easier if you use scikit-learn pipelines. When using cross-validation, it's even more critical that you do your preprocessing inside the pipeline!

**NOTE** Jeg er meget i tvivl om den her del. MEN! Det lyder til at man kan forhindre alt det her, hvis at man "blot" sørger for at al ens preprocessing sker inden i ens pipeline.

## Conclusion

Data leakage kan være en meget dyr fejl i mange data science applikationer. Forsigtig seperering af træning og validerings data kan forhindre train-test contamination, og pipelines kan hjælpe med at implementere denne seperation. Ligeledes kan en kombination af forsigtighed, common sense, og data exploration hjælpe med at identificere target leakage.

# Done.

Med det, så er jeg igennem kurset "Intermediate Machine Learning". <br/>
https://www.kaggle.com/learn/intermediate-machine-learning