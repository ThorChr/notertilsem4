# Introduction to Deep Learning

Denne notebog vil være anderledes end de andre. Jeg vil forsøge at complete et kursus, og så bagefter tænke over hvad jeg lærte, og skrive de ting ned. Fremfor at skrive ting ned imens jeg laver dem, som jeg gjorde før.

# A Single Neuron

Hvad er en neuron? En neuron er den mest fundamentale komponent af et neural network. En neuron har dens input, og hver input har deres egen weight. Weight er noget man skal gange input med, og resultatet af det er hvad der rammer ens neuron. <br/>
Derudover så har ens neuron også et bias, hvilket er en anden type af weight. Den har ingen input data, så dens værdi er bare whatever den er sat til. Bias giver mulighed for en neuron at ændre på output uafhængigt af dens inputs. <br/>
De ting lagt sammen giver output. Det kan blive opsummeret som ```y = wx + b```


## Create a network with 1 linear unit

Hvad er et unit? Unit er basically ens output. Så det her betyder lave et netværk der giver et lineart svar.

Koden for det er:

```python
from tensorflow import keras
from tensorflow.keras import layers

# Create a network with 1 linear unit
model = keras.Sequential([
    layers.Dense(units=1, input_shape=[3])
])
```

Input shape indikere hvor mange inputs vores neuron har. Units er hvor mange outputs.

## Ekstra stuff

Vores model objekt har en masse informationer. Den kan man skaffe ved at sige ```model.attribute``` Et eksempel er nedenstående kode for at få fat på vores models weight.

```python
w, b = model.weights

print("Weights\n{}\n\nBias\n{}".format(w, b))
```

# Deep Neural Networks

> Add hidden layers to your network to uncover complex relationships.

Neural networks organisere typisk deres neurons i layers. Når man samler flere linear units sammen, der alle har et fælles set af inputs, så har man et **dense** layer.

![Dense layer](https://i.imgur.com/F8RBC47.png)

Et hav af dense layers med intet andet, er ikke bedre end et enkelt dense layer. Dense layers i sig selv kan aldrig få os væk fra verdenen af "lines and planes". Man har brug for noget *nonlinear*. **activation functions**

## Activation functions

En activation function er en funktion vi applier til hver eneste af et lags outputs (dets activations). Den mest udbredte er *the rectifier function max(0, x)* <br/>
Den tager basically og sørger for  at en graf aldrig kan være negativ. Den tager den højeste værdi om det så er 0, eller x.

Man kan stacke dense layers med en *ReLu function* og få et "fully-connected" network der ser sådan her ud: 

![fully-connected dense layers with ReLu functions](https://i.imgur.com/vOZQ0fP.png)

For at lave ovenstående model, skal man bruge følgende kode:

```python
from tensorflow import keras
from tensorflow.keras import layers

model = keras.Sequential([
    # the hidden ReLU layers
    layers.Dense(units=4, activation='relu', input_shape=[2]),
    layers.Dense(units=3, activation='relu'),
    # the linear output layer 
    layers.Dense(units=1),
])
```

Sequential modellen der gøres brug af, vil forbinde en liste af layers fra først til sidst. Det første lag får input, det sidste lag producere output.

# Stochastic Gradient Descent

> Use Keras and Tensorflow to train your first neural network.

https://www.kaggle.com/code/ryanholbrook/stochastic-gradient-descent

At træne et neural network betyder at man justerer dets *weights* i sådan en facon at det kan transformere features om til at være et target.

For at træne et neural network skal man have 3 bestemte ting:

1. Dataset at arbejde med.
2. En "loss function" der måler hvor gode netværkets predictions er.
3. En "optimizer" der fortæller netværket hvordan det skal ændre dens weights.

## Loss function

> We've seen how to design an architecture for a network, but we haven't seen how to tell a network what problem to solve. This is the job of the loss function.

En loss function måler uligheden mellem targets sande værdier og værdierne fra vores predictions.

Der eksisterer forskellige loss functions. Til *regression* problemer hvor målet er at predicte en numerisk værdi, så er det ret normalt at gære brug af MAE. Mean Absolute Error.

## Optimizer

> We've described the problem we want the network to solve, but now we need to say how to solve it. This is the job of the optimizer. The optimizer is an algorithm that adjusts the weights to minimize the loss.

Basically alle optimizers der er brugt i Deep Learning falder ind under familien af **stochastic gradient descent**. Disse er iterative algoritmer der træner netværket i steps. One step of training looks like this:

1. Tag noget trænings data, og lav nogle predictions.
2. Mål loss mellem predictions og sande værdier.
3. Juster netværkets weights, så loss værdien bliver mindre.

Så bare bliv ved, om og om, indtil ens loss er så lille som man nu ønsker (eller indtil det ikke decreaser mere).

Hver iteration's trænings data bliver kaldet for **minibatch** (eller også bare forkortet til batch), hvorimod en fuld runde af trænings dataen bliver kaldt for en **epoch**. Antallet af epochs man træner for, er altså hvor mange gange netværket vil se hvert trænings eksempel.

## Learning rate and batch size

Disse to parametre er nogle af dem som har den største effekt på et netværk. 

> Fortunately, for most work it won't be necessary to do an extensive hyperparameter search to get satisfactory results. Adam is an SGD algorithm that has an adaptive learning rate that makes it suitable for most problems without any parameter tuning (it is "self tuning", in a sense). Adam is a great general-purpose optimizer.

## Adding loss and optimizer

For at gøre brug af loss og optimizer på vores netværk, gør man følgende:

```python
model.compile(
    optimizer="adam",
    loss="mae",
)
```

> Notice that we are able to specify the loss and optimizer with just a string. You can also access these directly through the Keras API -- if you wanted to tune parameters, for instance -- but for us, the defaults will work fine.

Så hvis man vil justere på parametre, skal man tilgå det vha. objektet der er tilsvarende de forskellige ting, og ikke kun en string værdi.

Man vil altså lave sit netværk, hvor man sætter det op til at have X antal layers med X antal neurons osv. <br/>
Efter man har gjort det, så kan man skrive den ovenstående kode.

## Training

Når man så ønsker at træne ens model, skal man skrive følgende kode:

```python
history = model.fit(
    X_train, y_train,
    validation_data=(X_valid, y_valid),
    batch_size=256,
    epochs=10,
)
```

history er bare variablen vi gemmer predictions i.
X_train og y_train er variabler tidligere lavet ud fra dataset.
batch_size og epochs blev der snakket om tidligere.

### Plotting

fit metoden holder styr på vores loss i løbet af træningen af modellen. Dette kan man konvertere til en pandas dataframe og så plotte det ud i en graf for visuelt at se hvordan det gik med træningen.

```python
import pandas as pd

# convert the training history to a dataframe
history_df = pd.DataFrame(history.history)
# use Pandas native plot method
history_df['loss'].plot()
```

# Overfitting and Underfitting

Disse to er også tidligere beskrevet i andre note filer.

De handler basically om hvorvidt ens model, eller well netværk, har fået lov til at træne i lang nok tid. Hvis den ikke har, så er den underfitted.

Hvis den har trænet i for lang tid, så er den blevet alt for præcis til præcist validations dataen, og ikke real-world data, og så er den overfitted.

For at undgå dette, kan man gøre brug af ```early stopping``` hvilket stopper træningen så snart at vores model begynder at blive overfitted. Det betyder at med den ting, undgår man både over- og underfitting. 

## Adding early stopping

I Keras så inkludere man early stopping igennem et callback. Et callback er bare en function man vil have der bliver kørt every so often, imens netværket træner. Early stopping vil køre ved hver epoch.

For at tilføje early stopping til vores netværk gøres følgende:

```python
from tensorflow.keras.callbacks import EarlyStopping

early_stopping = EarlyStopping(
    min_delta=0.001, # minimium amount of change to count as an improvement
    patience=20, # how many epochs to wait before stopping
    restore_best_weights=True,
)
```

> These parameters say: "If there hasn't been at least an improvement of 0.001 in the validation loss over the previous 20 epochs, then stop the training and keep the best model you found." It can sometimes be hard to tell if the validation loss is rising due to overfitting or just due to random batch variation. The parameters allow us to set some allowances around when to stop.

Nu har man sat vores early stopping op, for så at gøre brug af det, skriver man følgende kode for at træne modellen:

```python
history = model.fit(
    X_train, y_train,
    validation_data=(X_valid, y_valid),
    batch_size=256,
    epochs=500,
    callbacks=[early_stopping], # put your callbacks in a list
    verbose=0,  # turn off training log
)
```

